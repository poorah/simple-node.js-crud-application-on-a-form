const express = require('express');
var bodyParser = require('body-parser');
const { body } = require('express-validator');
const formController = require('./form.controller');


const formRouter = express.Router();

//کاستومایز کردن پیام های ولیدیشن
const dataValidator = () => [
    body('fname').notEmpty().withMessage('fname is empty!'),
    body('lname').notEmpty().withMessage('lname is empty!'),
    body('ncode').notEmpty().withMessage('ncode is empty!'),
    // ولیدیتور کد ملی جداگانه در کنترلر چک میکند
    body('phone').isMobilePhone('fa-IR').withMessage('phone is not valid!'),
];

// تابعی که دیتا های ورودی را به جیسون پارس میکند
var urlencodedParser = bodyParser.urlencoded({ extended: false });


// روتر ایندکس
formRouter.get('/',
    formController.index);




// create روتر
formRouter.post('/createUser',
    urlencodedParser,
    dataValidator(),
    formController.createUser);



// edit روتر
formRouter.get('/editUser/:userId', // همان کد ملی است userId  
    formController.editUser);




// update روتر
formRouter.put('/updateUser/:userId', // همان کد ملی است userId  
    urlencodedParser,
    dataValidator(),
    formController.updateUser);




//delete روتر
formRouter.delete('/deleteUser/:userId',  // همان کد ملی است userId 
    formController.deleteUser);


    
module.exports = formRouter;