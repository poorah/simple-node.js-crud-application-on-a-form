const User = require('../../models/form.mongo');
const checkCodeMeli = require('../../middlewears/ncode.middlewear');

//!پکیج اکسپرس ولیدیتور برای ولیدیت کردن داده ها
const { validationResult } = require('express-validator');


//صفحه اصلی
function index(req, res) {
    res.status(200).render('form');
}



// create عملیات
async function createUser(req, res) {

    // ولیدیت کردن داده های ورودی 
    var errors = validationResult(req);

    //ولیدیت کردن کد ملی
    if (!checkCodeMeli(req.body.ncode)) {
        return res.status(400).json({ error: 'invalid ncode' });
    }

    // اگه مشکلی وجود داشت ارور ها رو با فرمت جیسون میفرسته سمت کلاینت
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    //چک کردن اینکه آیا فردی با این کد ملی قبلا ثبت نام کرده یا نه
    var user = await User.findOne({ ncode: req.body.ncode });
    if (user !== null) {
        return res.status(400).json({ errors: 'User already registered' });
    }

    // اگه داده های ورودی اکی بودن
    const newUser = new User({
        fname: req.body.fname,
        lname: req.body.lname,
        ncode: req.body.ncode,
        phone: req.body.phone,
    });
    // چک کردن ارور های حین سیو کردن
    newUser.save(function (err) {
        if (err) return handleError(err);
    });

    return res.status(200).json(newUser);
}




// عملیات ادیت
async function editUser(req, res) {

    var userId = req.params.userId;

    // !بررسی اینکه این آیدی وجود داره یا نه
    var user = await User.findOne({ ncode: userId });
    if (user === null) {
        return res.status(404).json({ error: 'user was not found!' });
    }


    //داده های کاربر رو به صفحه مربوطه میفرسته تا لازم نباشه از اول همه رو وارد کنه!
    res.status(200).render('formEdit', {
        id: user.id,
        fname: user.fname,
        lname: user.lname,
        ncode: user.ncode,
        phone: user.phone,
    });
}




//عملیات آپدیت
async function updateUser(req, res) {

    // ولیدیت کردن داده های ورودی 
    const errors = validationResult(req);

    //ولیدیت کردن کد ملی
    if (!checkCodeMeli(req.body.ncode)) {
        return res.status(400).json({ error: 'invalid ncode' });
    }

    // اگه مشکلی وجود داشت ارور ها رو با فرمت جیسون میفرسته سمت کلاینت
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // اگه داده ها اکی بود
    User.updateOne({ ncode: req.body.ncode }, {
        fname: req.body.fname,
        lname: req.body.lname,
        ncode: req.body.ncode,
        phone: req.body.phone,
    }, function (err) {
        if (err) return handleError(err);
    });

    return res.status(200).json(await User.findOne({ ncode: req.body.ncode }));
}



//delete عملیات
async function deleteUser(req, res) {

    //چک کردن اینکه آیا کاربری با این کد ملی وجو دارد یا نه
    const user = await User.findOne({ ncode: req.params.userId });
    if(!user) {
        return res.status(404).json({error: 'user not found!'});
    }

    // اگر وجود داشت...
    await User.deleteOne({ ncode: req.params.userId });
    return res.status(202).json({result: 'successful delete!'});
}



module.exports = {
    index,
    createUser,
    editUser,
    updateUser,
    deleteUser,
};