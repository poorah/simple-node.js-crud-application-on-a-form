//داده های داخل دیتابیس
const User = require('../../models/form.mongo');

// برگرداندن تمام کاربران
async function getUsers(req, res) {
    res.status(200).json(await User.find({}));
}

module.exports = {
    getUsers,
};