const express = require('express');
const usersController = require('./users.controller');

const usersRouter = express.Router();


// نمایش کل یوزر ها در فرمت جیسون
usersRouter.get('/users',usersController.getUsers);


module.exports = usersRouter;