APIs:
    
    GET All users:

        http://localhost:3000/users
    
---------------------------------------------

    GET Index:

        http://localhost:3000/

----------------------------------------------

    POST create new user:

        http://localhost:3000/createUser

            Mock JSON value to be sent:
                {
                "fname": "test",
                "lname": "test",
                "ncode": "0737594098",
                "phone": "09121236548"
                }

-----------------------------------------------

    GET edit page with prefilled values :

        http://localhost:3000/editUser/:ncode

------------------------------------------------

    PUT updating Users data :

        http://localhost:3000/updateUser/:ncode

            Mock JSON value to be sent :

                {
                "fname": "test",
                "lname": "test",
                "ncode": "0737594098",
                "phone": "09121236548"
                }

-------------------------------------------------

    DELETE user :

        http://localhost:3000/deleteUser/:ncode

---------------------------------------------------