const path = require('path');
const express = require('express');
const formsRouter = require('../routers/form/form.router');
const usersRouter = require('../routers/users/users.router');


const app = express();

// ها از تمپلیت انجین اچ بی اس استفاده شد view برای
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '..', 'views'));


app.use(express.json());

//سیستم لاگ ساده که مدت زمان انجام هر ریکویست را محاسبه میکند
app.use((req, res, next) => {
    const start_time = Date.now();
    next();
    const end_time = Date.now();

    console.log(`${req.method} ${req.baseUrl}${req.url} ${end_time - start_time} ms`);
});

// روتر های مربوط به فرم ها
app.use(formsRouter);

//روتر های مربوط به کاربر
app.use(usersRouter);

module.exports = app;