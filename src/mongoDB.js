require('dotenv').config();
const mongoose = require('mongoose');

//اتصال به مانگو دی بی
async function connectToMongo() {

    const MONGO_URL = process.env.MONGO_URL;

    await mongoose.connect(MONGO_URL).then(() => {
        console.log('MONGO connected!');
    });
}


module.exports = {
    connectToMongo,
};