const http = require('http');
const app = require('./app');
const { connectToMongo } = require('./mongoDB');

const server = http.createServer(app);

// 😎عوض کردن پورت ها رو یاد گرفتم
const PORT = process.env.PORT;

async function start() {

    //اتصال به مانگودیبی
    await connectToMongo();

    //شروع سرور
    server.listen(PORT, () => {
        console.log(`listening to port: ${PORT}...`);
    });
}

start();
